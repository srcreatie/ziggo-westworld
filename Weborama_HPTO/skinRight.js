/**
 * Smart Right Skin
 * @author Weborama NL
 * @version 1.0.4
 * 2017-08-04
 */

// Width of visible area, max en original width, sticky scrolling vars
var visibleWidth, originalWidth, maxWidth, stickyScrollLimit, stickyScrolling, scroll_timeout;
// This object will hold the current website specifications (json).
var siteObject = {};
// Content holder.
var scalableContent;
// Resize every # milli-secs max (#SharedBG)
var resizeTimeout = 25, resizeTimer = 0;

/** Current HTML is loaded */
screenad.onPreloadComplete = function() {
  scalableContent = document.getElementById('scalableContent');
  originalWidth = maxWidth = scalableContent.getBoundingClientRect().width;
  onInit();
};

/**
 * Setup Screenad Positioning. Check if Scalable Content and Shared BG are used.
 * Shared function handler
 */
screenad.shared.setPosition = function(siteObj) {
  siteObject = siteObj;
  // Main screenad settings
  screenad.setSize(640, 1500);
  screenad.setAlignment('center', siteObject.skins_valign);
  screenad.setOffset((Math.floor(parseInt(siteObject.width) / 2) + 320) + parseInt(siteObject.skins_offsetx), parseInt(siteObject.skins_offsety));
  if (siteObject.skins_sticky == 'scroll') {
    screenad.setSticky(false); // First false to capture original vertical position
  } else {
    screenad.setSticky(siteObject.skins_sticky);
  }
  if (siteObject.skins_zindex !== undefined && siteObject.skins_zindex.length > 0) {
    screenad.setZIndex(parseInt(siteObject.skins_zindex));
  }
  screenad.position();
  // Scalable content?
  if (scaleContent) {
    screenad.onResize = updateScalableContent;
    updateScalableContent();
  }
  // Add sticky on scroll?
  if (siteObject.skins_sticky == 'scroll') {
    setTimeout(addStickyOnScroll, 100);
  }
  // Let content/animation start
  setTimeout(onStart, 200);
};

/** Checks resolution and adapts scalable content */
function updateScalableContent() {
  var contentWidth, contentHeight;
  if (scaleContent) {
    contentWidth = (Math.ceil(screenad.browserwidth * screenad.zoom) - parseInt(siteObject.width)) / 2 + parseInt(siteObject.skins_offsetx);
    scaleAmount = contentWidth / originalWidth;
    if (contentWidth >= maxWidth) {
      scaleAmount = 1;
    } else if (contentWidth < minWidth) {
      scaleAmount = minWidth / originalWidth;
    }
    scalableContent.style.transform = 'scale(' + scaleAmount + ',' + scaleAmount + ')';
  }
}

/** Show Shared Background - Skins Version */
screenad.shared.setSharedBg = function(bgObj) {
  var xOffset=0, bgHolder=document.getElementById('sharedBackground');
  if (!bgHolder) return;
  bgHolder.style.position = 'absolute';
  bgHolder.style.width = screenad.browserwidth+'px';
  bgHolder.style.height = '100%';
  bgHolder.style.left = '0px';
  bgHolder.style.top = '0px';
  if (bgHolder.style.backgroundImage == '')
    bgHolder.style.backgroundImage = 'url("'+bgObj.image+'")';
  if (bgObj.width != 'auto')
    xOffset = (screenad.browserwidth - bgObj.width)/2;
  bgHolder.style.backgroundSize = (bgObj.width=='auto'? screenad.browserwidth : bgObj.width)+'px auto';
  bgHolder.style.backgroundPositionX = Number(0 + screenad.pagex + xOffset)+'px';
  bgHolder.style.backgroundPositionY =  (Number(0 + screenad.pagey - bgObj.yOffset) > 0)? '0px' : Number(0 + screenad.pagey - bgObj.yOffset)+'px';
  bgHolder.style.backgroundRepeat = 'no-repeat';
  bgHolder.style.backgroundColor = bgObj.color;
}

function addStickyOnScroll() {
  // If not needed. Use regular Sticky
  if (siteObject.skins_valign == 'top' && siteObject.skins_offsety == '0') {
    screenad.setSticky(true);
    screenad.position();
    return;	
  }
  stickyScrollLimit = 0 - screenad.pagey + screenad.scrolly;
  if (typeof stickyScrollLimit == 'number') {
    screenad.addEventListener( screenad.SCROLL, scrollHandler);
    scrollHandler();
  }
}

/** Handles scroll event. Sets stickyness on scroll if needed */
function scrollHandler() {
  // Set 100 milliseconds as minimum interval between position commands
  if (scroll_timeout != null) {
    return;
  } else {
    scroll_timeout = setTimeout(function(){ scroll_timeout = null; scrollHandler(); }, 100);
  }
  if (screenad.pagey > 0 && !stickyScrolling) {
    // Stick to the top
    stickyScrolling = true;
    screenad.setSticky(true);
    screenad.setAlignment('center', 'top');
    screenad.setOffset((Math.floor(parseInt(siteObject.width) / 2) + 320) + parseInt(siteObject.skins_offsetx), 0);
    screenad.position();
  } else if (screenad.scrolly <= stickyScrollLimit && stickyScrolling) {
    // Go back to original vertical position
    stickyScrolling = false;
    screenad.setSticky(false);
    screenad.setAlignment('center', siteObject.skins_valign);
    screenad.setOffset((Math.floor(parseInt(siteObject.width) / 2) + 320) + parseInt(siteObject.skins_offsetx), parseInt(siteObject.skins_offsety));
    screenad.position();
  }
}
