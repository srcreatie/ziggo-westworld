function setElements(callback) {

    config = {};
    config.bannerWidth = 336;
    config.bannerHeight = 280;

    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }

    function enablerInitHandler() {

        if (Enabler.isPageLoaded()) {
            pageLoadedHandler();
        } else {

            Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED,
                pageLoadedHandler);
        }
    }

    function pageLoadedHandler() {
        addClicktag(dd.exit_url);
        loadFont();

        function loadFont() {
            sr.font.add([ asset('bold.woff'),
                asset('semibold.woff')
            ], add);
        }

        function add() {
            ___("bg")
                .image(dd.background_336x280, { width: 336, height: 280, wrap: true })
                .position({top:0, left:0});

            var video = document.createElement('video');
                    video.src = Enabler.getUrl(asset("336x280.mp4"));
                    video.setAttribute('id', 'westworldVideo');
                    video.setAttribute('autoPlay', true);                
                    video.playsInline = true;
                    video.setAttribute('muted', true);                
                    video.setAttribute('playsinline', 'playsinline');
                    video.setAttribute('webkit-playsinline', 'webkit-playsinline');
                    video.setAttribute('poster', asset("poster-336x280.jpg"));
                    enableInlineVideo(video);
                    video.play();

            ___("h1")
                .text(dd.copy.h1, {maxFs:36, width:310,  webfont:"bold", color:dd.style.headlineColor})
                .style({ greensock: { rotation:-3 } })
                .position({top:40, left:15 });
            
            ___("h2")
                .text(dd.copy.h2, {maxFs:24, width:280,  webfont:"semibold", color:dd.style.headlineColor})
                .style({ greensock: {rotation:-3 } })
                .position({ push:{ el:__("h1"), bottom:-5 }, left:20 });
            
            ___("cta")
                .text(dd.cta, { addClass:"cta", webfont:"semibold", fontSize:18, color:"#fff" })
                .position({ centerX:0, bottom:90 });    

            ___("logo")
                .image(asset("logo.png"), { width:117, height:68, fit:true })
                .position({ right:0, bottom:0 });

            __("video_holder").appendChild(video);

            sr.loading.done(callback);
        }
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        Enabler.exit('Background Exit Default');        
    }
}

module.exports.setElements = setElements;