function setElements(callback) {

    config = {};
    config.bannerWidth = 300;
    config.bannerHeight = 250;

    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }

    function enablerInitHandler() {

        if (Enabler.isPageLoaded()) {
            pageLoadedHandler();
        } else {

            Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED,
                pageLoadedHandler);
        }
    }

    function pageLoadedHandler() {
        addClicktag(dd.exit_url);
        loadFont();

        function loadFont() {
            sr.font.add([ asset('bold.woff'),
                asset('semibold.woff')
            ], add);
        }

        function add() {
            ___("bg")
                .image(dd.background_300x250, { width: 300, height: 250, wrap: true })
                .position({top:0, left:0});

            var video = document.createElement('video');
                    video.src = Enabler.getUrl(asset("300x250.mp4"));
                    video.setAttribute('id', 'westworldVideo');
                    video.setAttribute('autoPlay', true);                
                    video.playsInline = true;
                    video.setAttribute('muted', true);                
                    video.setAttribute('playsinline', 'playsinline');
                    video.setAttribute('webkit-playsinline', 'webkit-playsinline');
                    video.setAttribute('poster', asset("poster-300x250.jpg"));
                    enableInlineVideo(video);
                    video.play();

            ___("h1")
                .text(dd.copy.h1, {maxFs:32, width:270,  webfont:"bold", color:dd.style.headlineColor})
                .style({ greensock: { rotation:-3 } })
                .position({top:30, left:15 });
            
            ___("h2")
                .text(dd.copy.h2, {maxFs:24, width:250,  webfont:"semibold", color:dd.style.headlineColor})
                .style({ greensock: {rotation:-3 } })
                .position({ push:{ el:__("h1"), bottom:-5 }, left:20 });
            
            ___("cta")
                .text(dd.cta, { addClass:"cta", webfont:"semibold", fontSize:16, color:"#fff" })
                .position({ centerX:0, bottom:80 });    

            ___("logo")
                .image(asset("logo.png"), { width:106, height:61, fit:true })
                .position({ right:0, bottom:0 });

            __("video_holder").appendChild(video);

            sr.loading.done(callback);
        }
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        Enabler.exit('Background Exit Default');        
    }
}

module.exports.setElements = setElements;