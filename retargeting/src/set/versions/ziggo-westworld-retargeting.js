function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed 2";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.h1 = "Kijk Westworld nú";
    devDynamicContent.srFeed[0].copy.h2 = "Hoe je dit doet?";
    devDynamicContent.srFeed[0].copy.h2_sky = "Hoe je dit doet?";    
    devDynamicContent.srFeed[0].cta = "Check het hier";
    devDynamicContent.srFeed[0].cta_sky = "Check het <br />hier";
    devDynamicContent.srFeed[0].color = {};
    devDynamicContent.srFeed[0].style = {};
    devDynamicContent.srFeed[0].style.headlineColor = "#ffffff" ; 
    devDynamicContent.srFeed[0].background_300x250 = dimension("bg-300x250.jpg", "300x250");
    devDynamicContent.srFeed[0].background_336x280 = dimension("bg-336x280.jpg", "336x280");
    devDynamicContent.srFeed[0].background_120x600 = dimension("bg-120x600.jpg", "120x600");
    devDynamicContent.srFeed[0].background_160x600 = dimension("bg-160x600.jpg", "160x600");
    devDynamicContent.srFeed[0].background_728x90 = dimension("bg-728x90.jpg", "728x90");

    return devDynamicContent;
}

module.exports = setData;