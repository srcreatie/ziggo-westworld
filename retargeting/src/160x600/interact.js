function animate(callback) {

    setAnimation();

    function setAnimation() {
        videoStarted = false;

        __("creative").style.opacity = 1;
        var tl01 = new TimelineMax({ paused : true});     
        var tlRO = new TimelineMax({ paused : true});     

        tl01.staggerFrom([__("h1"), __("h2")], 0.2, {alpha:0, repeat:2, yoyo:true, repeatDelay:0.2}, 0.05);   
        tl01.to(__("cta"), 0.2, {scale:1.1, repeat:1, yoyo:true, repeatDelay:0.4, ease:Back.easeInOut.config(1.2), onComplete:rollover}, "+=0.4");

        tlRO.staggerTo([__("h1"), __("h2")], 0.2, {alpha:0, repeat:1, yoyo:true, repeatDelay:0.1}, 0.05, "rollover");   
        tlRO.to(__("cta"), 0.2, {scale:1.1, ease:Back.easeInOut.config(1.2)}, "rollover");


        document.getElementById('westworldVideo').addEventListener('ended',videoEnded,false); 
        document.getElementById('westworldVideo').addEventListener('playing',function(){
            videoStarted = true;
        },false);

        // Fallback is video doens't start playing
        TweenMax.to(this, 4, {onComplete:function(){            
            if(videoStarted === false){
                // Backup tween to test if video doesnt play
                TweenMax.to(__("video_holder"), 0.5, { alpha:0, x:-5, delay:10 });
            }
        }
        });

        function videoEnded(e){            
            TweenMax.to(__("video_holder"), 0.5, { alpha:0, x:-5, delay:0.1 });  
            tl01.play();         
        }

        function rollover(){
            __("banner").onmouseover = function(){       
                tlRO.play();
            };

            __("banner").onmouseout = function(){
                tlRO.reverse();
            };
        }

        if (srBanner.debug) {
            if (srBanner.debug && srBanner.pauseFrom) {
                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {
                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }


            if (srBanner && srBanner.backupImage) {
                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }
    }

    if (callback) {
        callback();
    }
}

module.exports.animate = animate;