function setElements(callback) {

    config = {};
    config.bannerWidth = 120;
    config.bannerHeight = 600;

    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }

    function enablerInitHandler() {

        if (Enabler.isPageLoaded()) {
            pageLoadedHandler();
        } else {

            Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED,
                pageLoadedHandler);
        }
    }

    function pageLoadedHandler() {
        addClicktag(dd.exit_url);
        loadFont();

        function loadFont() {
            sr.font.add([ asset('bold.woff'),
                asset('semibold.woff')
            ], add);
        }

        function add() {
            ___("bg")
                .image(dd.background_120x600, { width: 120, height: 600, wrap: true })
                .position({top:0, left:0});

            var video = document.createElement('video');
                    video.src = Enabler.getUrl(asset("120x600.mp4"));
                    video.setAttribute('id', 'westworldVideo');
                    video.setAttribute('autoPlay', true);                
                    video.playsInline = true;
                    video.setAttribute('muted', true);                
                    video.setAttribute('playsinline', 'playsinline');
                    video.setAttribute('webkit-playsinline', 'webkit-playsinline');
                    video.setAttribute('poster', asset("poster-120x600.jpg"));
                    enableInlineVideo(video);
                    video.play();

            ___("h1")
                .text(dd.copy.h1, {maxFs:22, width:110,  webfont:"bold", color:dd.style.headlineColor})
                .style({ greensock: { rotation:-3 } })
                .position({top:170, left:5 });
            
            ___("h2")
                .text(dd.copy.h2_sky, {maxFs:15, width:110,  webfont:"semibold", color:dd.style.headlineColor})
                .style({ greensock: {rotation:-3 }, css:"line-height:1.1;" })
                .position({ push:{ el:__("h1"), bottom:0 }, left:7 });
            
            ___("cta")
                .text(dd.cta_sky, { addClass:"cta", webfont:"semibold", fontSize:14, color:"#fff", textAlign:"center" })
                .position({ centerX:0, bottom:220 });    

            ___("logo")
                .image(asset("logo.png"), { width:106, height:61, fit:true })
                .position({ right:0, bottom:0 });

            __("video_holder").appendChild(video);

            sr.loading.done(callback);
        }
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {        
        Enabler.exit('Background Exit Default');        
    }
}

module.exports.setElements = setElements;