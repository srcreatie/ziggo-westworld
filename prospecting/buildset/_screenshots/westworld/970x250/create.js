function setElements(callback) {

    config = {};
    config.bannerWidth = 970;
    config.bannerHeight = 250;

    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }

    function enablerInitHandler() {

        if (Enabler.isPageLoaded()) {
            pageLoadedHandler();
        } else {

            Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED,
                pageLoadedHandler);
        }
    }

    function pageLoadedHandler() {
        addClicktag(dd.exit_url);
        loadFont();

        function loadFont() {
            sr.font.add([
                asset('semibold.woff')
            ], add);
        }

        function add() {
            ___("bg").image(dd.background_970x250, { width: 970, height: 250, fit: true })

            var video = document.createElement('video');
                    video.src = Enabler.getUrl(asset("970x250.mp4"));
                    video.id = "westworldVideo";
                    video.autoPlay = true;
                    video.controls = false;
                    video.playsInline = true;
                    video.muted = true;
                    video.loop = false;
                    video.setAttribute('playsinline', 'playsinline');
                    video.setAttribute('webkit-playsinline', 'webkit-playsinline');
                    video.poster = asset("970x250-poster.jpg");
                    enableInlineVideo(video);
                    video.play();


            ___("logo")
                .image(asset("logo.png"), { width:174, height:100, fit:true })
                .position({ right:0, bottom:0 })

            ___("h1")
            .text(dd.copy.h1, {maxFs:72, width:380,  webfont:"semibold", color:"#FFFFFF"})
            .style({ greensock: { rotation:-2 } })
            .position({top:15, left:20 });
        
        ___("h2")
            .text(dd.copy.h2_hi, {maxFs:28, width:360,  webfont:"semibold", color:"#FFFFFF"})
            .style({ greensock: {rotation:-3 } })
            .position({ push:{ el:__("h1"), bottom:0 }, left:45 });
        
        ___("h3")
            .text(dd.copy.h3, {maxFs:48, width:390,  webfont:"semibold", textAlign:"center", color:"#FFFFFF"})
            // .style({ css:"text-shadow:2px 5px 5px #ff77e0;" })
            .position({ top:60, left:450 });

        ___("banner>answer")
            .style({ width:320, height:60, css:"z-index:-100;", greensock: { opacity:0 } })
            .position({ centerX:30, bottom:25 });

            ___("answer>ja_glitch")
                .image(asset("ja_glitch.png"), { width:166, height:60, fit:true })
                .style({ greensock: { opacity:0 } })
                .position({ left:0 });

            ___("answer>nee_glitch")
                .image(asset("nee_glitch.png"), { width:166, height:60, fit:true })
                .style({ greensock: { opacity:0 } })
                .position({ left:160 });

            ___("answer>ja")
                .text("ja", { color:"#fff", background:"#000", webfont:"semibold", fontSize:45, textAlign:"center" })
                .style({ width:110, height:60 })
                .position({ left:0 });

            ___("answer>nee")
                .text("nee", { color:"#fff", background:"#000", webfont:"semibold", fontSize:45, textAlign:"center" })
                .style({ width:110, height:60 })
                .position({ left:160 });

        ___("vraag_h1")
            .text(dd.copy.vraag_h1, {maxFs:54, width:550,  webfont:"semibold", textAlign:"center", color:"#FFFFFF"})
            // .style({ css:"text-shadow:2px 5px 5px #ff77e0;" })
            .position({ top:30, centerX:0 });

        ___("vraag_h2")
            .text(dd.copy.vraag_h2, {maxFs:54, width:570,  webfont:"semibold", textAlign:"center", color:"#FFFFFF"})
            // .style({ css:"text-shadow:2px 5px 5px #ff77e0;" })
            .position({ top:30, centerX:0 });

        ___("vraag_h3")
            .text(dd.copy.vraag_h3, {maxFs:54, width:520,  webfont:"semibold", textAlign:"center", color:"#FFFFFF"})
            // .style({ css:"text-shadow:2px 5px 5px #ff77e0;" })
            .position({ top:30, centerX:0 });

        ___("left_corner")
            .style({ background:"#fff", width:140, height:60, greensock:{ opacity:0 }, css:"-webkit-border-bottom-right-radius: 15px; -moz-border-bottom-right-radius: 15px; border-bottom-right-radius: 15px;" })
            .position({ left:-5, top:-7 });

            ___("left_corner>v1")
                .text("Vraag 1", { color:"#000", webfont:"semibold", fontSize:28 })
                .position({ left:21, top:13 });

            ___("left_corner>v2")
                .text("Vraag 2", { color:"#000", webfont:"semibold", fontSize:28 })
                .style({ greensock:{ opacity:0 } })
                .position({ left:21, top:13 });

            ___("left_corner>v3")
                .text("Vraag 3", { color:"#000", webfont:"semibold", fontSize:28 })
                .style({ greensock:{ opacity:0 } })
                .position({ left:21, top:13 });

        ___("end_h1")
            .text(dd.copy.end_h1, {maxFs:54, width:320,  webfont:"semibold", textAlign:"center", color:"#FFFFFF"})
            // .style({ css:"text-shadow:2px 5px 5px #ff77e0;" })
            .position({ top:30, centerX:130 });

        ___("end_cta")
            .text(dd.cta, { addClass:"cta", webfont:"semibold", fontSize:19, color:"#fff" })
            .style({ greensock: { alpha:0 }})
            .position({ centerX:130, bottom:40 });    

        ___("blue")
            .image(dd.blue, { width: 970, height: 250, wrap: true })
            .style({ greensock: { opacity:0 } })
            .position({top:0, left:0});

        ___("banner>take_test")
            .text("Doe de test", { addClass:"cta", webfont:"semibold", fontSize:28, color:"#fff" })
            .style({ css:"opacity:0; z-index:-100;" })
            .position({el:__("h3"), centerX:0})
            .position({el:__("creative"), bottom:20 });

        __("video_holder").appendChild(video);

            sr.loading.done(callback);
        }
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (clickout == "default") {
            Enabler.exit('Background Exit Default');
        } else if(clickout == "mens"){
            Enabler.exit('Background Exit Mens');
        } else if(clickout == "robot"){
            Enabler.exit('Background Exit Robot');
        } else if(clickout == "mens_robot"){
            Enabler.exit('Background Exit Mens_Robot');
        }
    }
}

module.exports.setElements = setElements;