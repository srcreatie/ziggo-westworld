function setElements(callback) {

    config = {};
    config.bannerWidth = 300;
    config.bannerHeight = 600;

    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }

    function enablerInitHandler() {

        if (Enabler.isPageLoaded()) {
            pageLoadedHandler();
        } else {

            Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED,
                pageLoadedHandler);
        }
    }

    function pageLoadedHandler() {
        addClicktag(dd.exit_url);
        loadFont();

        function loadFont() {
            sr.font.add([
                asset('semibold.woff')
            ], add);
        }

        function add() {
            ___("bg").image(dd.background_300x600, { width: 300, height: 600, fit: true })

            var video = document.createElement('video');
                    video.src = Enabler.getUrl(asset("300x600.mp4"));
                    video.setAttribute('id', 'westworldVideo');
                    video.setAttribute('autoPlay', true);                
                    video.playsInline = true;
                    video.setAttribute('muted', true);                
                    video.setAttribute('playsinline', 'playsinline');
                    video.setAttribute('webkit-playsinline', 'webkit-playsinline');
                    video.setAttribute('poster', asset("300x600-poster.jpg"));
                    enableInlineVideo(video);
                    video.play();


            ___("logo")
                .image(asset("logo.png"), { width:174, height:100, fit:true })
                .position({ right:0, bottom:0 })

        ___("h1")
            .text(dd.copy.h1, {maxFs:52, width:270,  webfont:"semibold", color:"#FFFFFF"})
            .style({ greensock: { rotation:-2 } })
            .position({top:45, left:15 });
        
        ___("h2")
            .text(dd.copy.h2_hi, {maxFs:23, width:250,  webfont:"semibold", color:"#FFFFFF"})
            .style({ greensock: {rotation:-3 } })
            .position({ push:{ el:__("h1"), bottom:0 }, left:35 });
        
        ___("h3")
            .text(dd.copy.h3, {maxFs:35, width:255,  webfont:"semibold", textAlign:"center", color:"#FFFFFF"})
            // .style({ css:"text-shadow:2px 5px 5px #ff77e0;" })
            .position({ top:260, centerX:0 });

        ___("banner>answer")
            .style({ width:260, height:50, css:"z-index:-100;", greensock: { opacity:0 } })
            .position({ centerX:5, bottom:220 });

            ___("answer>ja_glitch")
                .image(asset("ja_glitch.png"), { width:146, height:50, fit:true })
                .style({ greensock: { opacity:0 } })
                .position({ left:0 });

            ___("answer>nee_glitch")
                .image(asset("nee_glitch.png"), { width:146, height:50, fit:true })
                .style({ greensock: { opacity:0 } })
                .position({ left:160 });

            ___("answer>ja")
                .text("ja", { color:"#fff", background:"#000", webfont:"semibold", fontSize:35, textAlign:"center" })
                .style({ width:90, height:50 })
                .position({ left:0 });

            ___("answer>nee")
                .text("nee", { color:"#fff", background:"#000", webfont:"semibold", fontSize:35, textAlign:"center" })
                .style({ width:90, height:50 })
                .position({ left:160 });

        ___("vraag_h1")
            .text(dd.copy.vraag_h1, {maxFs:34, width:260,  webfont:"semibold", textAlign:"center", color:"#FFFFFF"})
            // .style({ css:"text-shadow:2px 5px 5px #ff77e0;" })
            .position({ top:160, centerX:0 });

        ___("vraag_h2")
            .text(dd.copy.vraag_h2, {maxFs:34, width:260,  webfont:"semibold", textAlign:"center", color:"#FFFFFF"})
            // .style({ css:"text-shadow:2px 5px 5px #ff77e0;" })
            .position({ top:160, centerX:0 });

        ___("vraag_h3")
            .text(dd.copy.vraag_h3, {maxFs:34, width:260,  webfont:"semibold", textAlign:"center", color:"#FFFFFF"})
            // .style({ css:"text-shadow:2px 5px 5px #ff77e0;" })
            .position({ top:160, centerX:0 });

        ___("left_corner")
            .style({ background:"#fff", width:120, height:50, greensock:{ opacity:0 }, css:"-webkit-border-bottom-right-radius: 15px; -moz-border-bottom-right-radius: 15px; border-bottom-right-radius: 15px;" })
            .position({ left:-5, top:-7 });

            ___("left_corner>v1")
                .text("Vraag 1", { color:"#000", webfont:"semibold", fontSize:22 })
                .position({ left:21, top:13 });

            ___("left_corner>v2")
                .text("Vraag 2", { color:"#000", webfont:"semibold", fontSize:22 })
                .style({ greensock:{ opacity:0 } })
                .position({ left:21, top:13 });

            ___("left_corner>v3")
                .text("Vraag 3", { color:"#000", webfont:"semibold", fontSize:22 })
                .style({ greensock:{ opacity:0 } })
                .position({ left:21, top:13 });

        ___("end_h1")
            .text(dd.copy.end_h1, {maxFs:35, width:220,  webfont:"semibold", textAlign:"center", color:"#FFFFFF"})
            // .style({ css:"text-shadow:2px 5px 5px #ff77e0;" })
            .position({ top:260, centerX:0 });

        ___("end_cta")
            .text(dd.cta, { addClass:"cta", webfont:"semibold", fontSize:18, color:"#fff" })
            .style({ greensock: { alpha:0 }})
            .position({ centerX:0, top:360 });    

        ___("blue")
            .image(dd.blue, { width: 300, height: 600, wrap: true })
            .style({ greensock: { opacity:0 } })
            .position({top:0, left:0});

        ___("banner>take_test")
            .text("Doe de test", { addClass:"cta", webfont:"semibold", fontSize:18, color:"#fff" })
            .style({ css:"opacity:0; z-index:-100;" })
            .position({centerX:0, top:360});

        __("video_holder").appendChild(video);

            sr.loading.done(callback);
        }
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (clickout == "default") {
            Enabler.exit('Background Exit Default');
        } else if(clickout == "mens"){
            Enabler.exit('Background Exit Mens');
        } else if(clickout == "robot"){
            Enabler.exit('Background Exit Robot');
        } else if(clickout == "mens_robot"){
            Enabler.exit('Background Exit Mens_Robot');
        }
    }
}

module.exports.setElements = setElements;