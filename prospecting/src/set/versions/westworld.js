function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed 2";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.h1 = "Westworld";
    devDynamicContent.srFeed[0].copy.h2 = "kijk je alleen bij Ziggo";
    devDynamicContent.srFeed[0].copy.h2_hi = "kijk je alleen bij Ziggo<br /> Ook het nieuwe seizoen, vanaf 23 april";
    devDynamicContent.srFeed[0].copy.h3 = "Ben jij een mens of robot?";
    devDynamicContent.srFeed[0].copy.vraag_h1 = "Heb jij ooit getwijfeld aan je menselijkheid?";
    devDynamicContent.srFeed[0].copy.vraag_h2 = "Zijn al jouw gedachtes van jezelf?";
    devDynamicContent.srFeed[0].copy.vraag_h3 = "Heb jij déjà vu’s van een ander leven?";
    devDynamicContent.srFeed[0].copy.end_h1 = "Ben jij mens of robot?";
    devDynamicContent.srFeed[0].cta = "Bekijk hier jouw uitslag";
    devDynamicContent.srFeed[0].color = {};
    devDynamicContent.srFeed[0].style = {};
    devDynamicContent.srFeed[0].style.headlineColor = "#ffffff" ; 
    devDynamicContent.srFeed[0].background_300x250 = dimension("bg-300x250.jpg", "300x250");
    devDynamicContent.srFeed[0].background_970x250 = dimension("bg-970x250.jpg", "970x250");
    devDynamicContent.srFeed[0].background_300x600 = dimension("bg-300x600.jpg", "300x600");
    devDynamicContent.srFeed[0].background_320x240 = dimension("bg-320x240.jpg", "320x240");
    devDynamicContent.srFeed[0].background_320x480 = dimension("bg-320x480.jpg", "320x480");
    devDynamicContent.srFeed[0].blue = asset("blue.jpg"); 

    return devDynamicContent;
}

module.exports = setData;