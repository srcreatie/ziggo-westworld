function setElements(callback) {

    config = {};
    config.bannerWidth = 320;
    config.bannerHeight = 480;

    if (Enabler.isInitialized()) {
        enablerInitHandler();
    } else {
        Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
    }

    function enablerInitHandler() {

        if (Enabler.isPageLoaded()) {
            pageLoadedHandler();
        } else {

            Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED,
                pageLoadedHandler);
        }
    }

    function pageLoadedHandler() {
        addClicktag(dd.exit_url);
        loadFont();

        function loadFont() {
            sr.font.add([
                asset('semibold.woff')
            ], add);
        }

        function add() {
            ___("bg").image(dd.background_320x480, { width: 320, height: 480, wrap: true })

            var video = document.createElement('video');
                    video.src = Enabler.getUrl(asset("320x480.mp4"));
                    video.setAttribute('id', 'westworldVideo');
                    video.setAttribute('autoPlay', true);                
                    video.playsInline = true;
                    video.setAttribute('muted', true);                
                    video.setAttribute('playsinline', 'playsinline');
                    video.setAttribute('webkit-playsinline', 'webkit-playsinline');
                    video.setAttribute('poster', asset("320x480-poster.jpg"));
                    enableInlineVideo(video);
                    video.play();


            ___("logo")
                .image(asset("logo.png"), { width:106, height:61, fit:true })
                .position({ right:0, bottom:0 })

            ___("h1")
                .text(dd.copy.h1, {maxFs:48, width:250,  webfont:"semibold", color:dd.style.headlineColor})
                .style({ greensock: { rotation:-3 } })
                .position({top:15, left:20 })
            
            ___("h2")
                .text(dd.copy.h2_hi, {maxFs:21, width:240,  webfont:"semibold", color:dd.style.headlineColor})
                .style({ greensock: {rotation:-3 } })
                .position({ push:{ el:__("h1"), bottom:0 }, left:30 })
            
            ___("h3")
                .text(dd.copy.h3, {maxFs:32, width:230,  webfont:"semibold", textAlign:"center", color:dd.style.headlineColor})
                .position({ top:220, centerX:0 })

            ___("banner>answer")
                .style({ width:215, height:30, greensock: { opacity:0 }, css:"z-index:-100;"})
                .position({ centerX:0, bottom:180 })

                ___("answer>ja_glitch")
                    .image(asset("ja_glitch.png"), { width:64, height:26, fit:true })
                    .style({ greensock: { opacity:0 } })
                    .position({ left:0 })

                ___("answer>nee_glitch")
                    .image(asset("nee_glitch.png"), { width:64, height:26, fit:true })
                    .style({ greensock: { opacity:0 } })
                    .position({ left:160 })

                ___("answer>ja")
                    .text("ja", { color:"#fff", background:"#000", webfont:"semibold", fontSize:22, textAlign:"center" })
                    .style({ width:60, height:34 })
                    .position({ left:0, top:0})

                ___("answer>nee")
                    .text("nee", { color:"#fff", background:"#000", webfont:"semibold", fontSize:22, textAlign:"center" })
                    .style({ width:60, height:34 })
                    .position({ left:160 })

            ___("vraag_h1")
                .text(dd.copy.vraag_h1, {maxFs:26, width:270,  webfont:"semibold", textAlign:"center", color:dd.style.headlineColor})
                .position({ top:175, centerX:0 })

            ___("vraag_h2")
                .text(dd.copy.vraag_h2, {maxFs:26, width:270,  webfont:"semibold", textAlign:"center", color:dd.style.headlineColor})
                .position({ top:175, centerX:0 })

            ___("vraag_h3")
                .text(dd.copy.vraag_h3, {maxFs:26, width:270,  webfont:"semibold", textAlign:"center", color:dd.style.headlineColor})
                .position({ top:175, centerX:0 })

            ___("left_corner")
                .style({ background:"#fff", width:100, height:42, greensock:{ opacity:0 }, css:"-webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;" })
                .position({ left:-5, top:-7 })

                ___("left_corner>v1")
                    .text("Vraag 1", { color:"#000", webfont:"semibold", fontSize:18 })
                    .position({ left:21, top:12 })

                ___("left_corner>v2")
                    .text("Vraag 2", { color:"#000", webfont:"semibold", fontSize:18 })
                    .style({ greensock:{ opacity:0 } })
                    .position({ left:21, top:12 })

                ___("left_corner>v3")
                    .text("Vraag 3", { color:"#000", webfont:"semibold", fontSize:18 })
                    .style({ greensock:{ opacity:0 } })
                    .position({ left:21, top:12 })

            ___("end_h1")
                .text(dd.copy.end_h1, {maxFs:32, width:230,  webfont:"semibold", textAlign:"center", color:dd.style.headlineColor})
                .position({ top:220, centerX:0 })

            ___("end_cta")
                .text(dd.cta, { addClass:"cta", webfont:"semibold", fontSize:16, color:"#fff" })
                .style({ greensock: { alpha:0 } })
                .position({ centerX:0, bottom:130 });    

            ___("blue")
                .image(asset('blue.jpg'), { width: 320, height: 480, wrap: true })
                .style({ greensock: { opacity:0 } })

            ___("banner>take_test")
                .text("Doe de test", { addClass:"cta", webfont:"semibold", fontSize:16, color:"#fff" })
                .style({ css:"opacity:0; z-index:-100" })
                .position({ centerX:0, bottom:130 });

            __("video_holder").appendChild(video);

            sr.loading.done(callback);
        }
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (clickout == "default") {
            Enabler.exit('Background Exit Default');
        } else if(clickout == "mens"){
            Enabler.exit('Background Exit Mens');
        } else if(clickout == "robot"){
            Enabler.exit('Background Exit Robot');
        } else if(clickout == "mens_robot"){
            Enabler.exit('Background Exit Mens_Robot');
        }
    }
}

module.exports.setElements = setElements;