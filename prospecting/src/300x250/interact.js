function animate(callback) {

    setAnimation();

    function setAnimation() {

        // Set clickout var
        clickout = "default";
        mens = 0;
        robot = 0;
        mens_robot = 0;

        videoStarted = false;

        __("creative").style.opacity = 1;

        var tl01 = new TimelineMax();
        var tlbuttons = new TimelineMax({ repeat:-1 });
        
        var tlvraag1 = new TimelineMax({ paused:true });
        var tlvraag2 = new TimelineMax({ paused:true });
        var tlvraag3 = new TimelineMax({ paused:true });
        
        var tlEnd = new TimelineMax({ paused:true });

        var transformHeader = "center right";

        tlbuttons.to([__("ja"), __("nee")], 0, { alpha:0 }, "buttons");
        tlbuttons.to([__("ja_glitch"), __("nee_glitch")], 0, { alpha:1 }, "buttons");
        tlbuttons.to(this, 0.1, {});
        tlbuttons.to([__("ja"), __("nee")], 0, { alpha:1 }, "buttonsOut");
        tlbuttons.to([__("ja_glitch"), __("nee_glitch")], 0, { alpha:0 }, "buttonsOut");
        tlbuttons.to(this, 3, {});

        tl01.set([__("h1"), __("h2")], {alpha:0, rotation:-20, y:360, x:20, transformOrigin:transformHeader});
        tl01.to(__("h1"), 1.4, { alpha:1, rotation:-3, y:0, x:0, ease:Back.easeInOut.config(0.8) }, "shadow");
        tl01.to(__("h2"), 1.4, {alpha:1, rotation:-3, y:0, x:0, ease:Back.easeInOut.config(0.8) }, "shadow+=0.2");
        tl01.from(__("h3"), 0.5, {alpha:0, scale:0, ease:Back.easeOut}, "shadow");
        
        tlvraag1.to(this, 0.5, {});
        tlvraag1.to([__("h1"), __("h2"), __("h3"), __("take_test")], 0, { opacity:0, onComplete:function(){ __("take_test").style.display = "none"; } });
        tlvraag1.to(__("left_corner"), 0, { opacity:1 });
        tlvraag1.to(__("v1"), 0, { opacity:1 });
        tlvraag1.to(__("blue"), 0.1, { opacity:0 });
        tlvraag1.from(__("vraag_h1"), 1.4, { scale:0, ease:Back.easeInOut.config(0.8) }, "-=0.5");
        tlvraag1.to(__("answer"), 0.5, { opacity:1 });
        
        tlvraag2.to(__("answer"), 0.1, { opacity:0 }, "change");
        tlvraag2.to(this, 0.5, {});
        tlvraag2.to([__("vraag_h1")], 0.2, { opacity:0 });
        tlvraag2.to(__("v1"), 0, { opacity:0 });
        tlvraag2.to(__("v2"), 0, { opacity:1 });
        tlvraag2.to(__("answer"), 0, { opacity:1 });
        tlvraag2.to(__("blue"), 0.1, { opacity:0 });
        tlvraag2.from(__("vraag_h2"), 1.4, { scale:0, ease:Back.easeInOut.config(0.8) }, "-=0.5");
        
        tlvraag3.to(__("answer"), 0.1, { opacity:0 }, "change1");
        tlvraag3.to(this, 0.5, {});
        tlvraag3.to([__("vraag_h2")], 0.2, { opacity:0 });
        tlvraag3.to(__("v2"), 0, { opacity:0 });
        tlvraag3.to(__("v3"), 0, { opacity:1 });
        tlvraag3.to(__("answer"), 0, { opacity:1 });
        tlvraag3.to(__("blue"), 0.1, { opacity:0 });
        tlvraag3.from(__("vraag_h3"), 1.4, { scale:0, ease:Back.easeInOut.config(0.8) }, "-=0.5");
        
        tlEnd.to(__("left_corner"), 0, { opacity:0 });
        tlEnd.to([__("vraag_h2"), __("answer")], 0.2, { opacity:0 });
        tlEnd.to(this, 0.5, {});
        tlEnd.to(__("blue"), 0.1, { opacity:0 });
        tlEnd.from(__("end_h1"), 1.4, { scale:0, ease:Back.easeInOut.config(0.8), onStart:function(){
            TweenMax.to(__("answer"), 0, {css:{zIndex:-100}});
            if(mens > robot){
                clickout = "mens";
            } else if(robot > mens) {
                clickout = "robot";
            } else if(mens == robot){
                clickout = "mens_robot";
            } else {
                clickout = 'default';
            }
        } }, "-=0.5");
        tlEnd.to(__("end_cta"), 0.5, { alpha:1 });

        var take_test = document.getElementById("take_test");
        take_test.onclick = function() { 
            TweenMax.to(__("answer"), 0, {css:{zIndex:1}});
            tlvraag1.play(0);
        };

        question = 1;

        var ja = document.getElementById("ja");
        ja.onclick = function() { 
            // if (question == 3) {
            //     tlEnd.play(0);
            //     robot++;
            // }
            if (question == 2) {
                // tlvraag3.play(0);
                tlEnd.play(0);
                question = 3;  
                mens++;
                Enabler.counter('Question2-JA');
            }
            if (question == 1) {
                tlvraag2.play(0);
                question = 2;
                robot++;
                Enabler.counter('Question1-JA');
            }
        };

        var nee = document.getElementById("nee");
        nee.onclick = function() { 
            // if (question == 3) {
            //     tlEnd.play(0);
            //     mens++;
            // }
            if (question == 2) {
                // tlvraag3.play(0);
                tlEnd.play(0);
                question = 3; 
                robot++;
                Enabler.counter('Question2-NEE');
            }
            if (question == 1) {
                tlvraag2.play(0);
                question = 2;
                mens++;
                Enabler.counter('Question1-NEE');
            }
        };

        document.getElementById('westworldVideo').addEventListener('ended',videoEnded,false); 
        document.getElementById('westworldVideo').addEventListener('playing',function(){
            videoStarted = true;
        },false);

        // Fallback is video doens't start playing
        TweenMax.to(this, 4, {onComplete:function(){            
            if(videoStarted === false){
                // Backup tween to test if video doesnt play
                TweenMax.to(__("video_holder"), 0.5, { alpha:0, x:-5, delay:10 });
                TweenMax.to(__("take_test"), 0.5, { alpha:1, delay:10 });
            }
        }
        });

        function videoEnded(e){            
            TweenMax.to(__("video_holder"), 0.5, { alpha:0, x:-5, delay:0.1 });
            TweenMax.to(__("take_test"), 0, {css:{zIndex:1}, delay:0.5});
            TweenMax.to(__("take_test"), 0.5, { alpha:1, delay:0.6 });            
        }

        if (srBanner.debug) {
            if (srBanner.debug && srBanner.pauseFrom) {
                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {
                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }


            if (srBanner && srBanner.backupImage) {
                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }
    }

    if (callback) {
        callback();
    }
}

module.exports.animate = animate;